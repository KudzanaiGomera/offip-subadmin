import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AccountComponent } from './pages/account/account.component';
import { ReportDataComponent } from './pages/report-data/report-data.component';
import { ReportModalComponent } from './pages/report-modal/report-modal.component';
import { UploadReportComponent } from './pages/upload-report/upload-report.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },

  {
    path: 'login',
    component: LoginComponent,
  },

  {
    path: 'report',
    component: ReportDataComponent,
  },

  {
    path: 'report/:name',
    component: ReportModalComponent,
  },

  {
    path: 'account',
    component: AccountComponent,
  },

  {
    path: 'upload',
    component: UploadReportComponent,
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
