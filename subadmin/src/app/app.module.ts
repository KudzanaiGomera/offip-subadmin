import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AgGridModule } from 'ag-grid-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NotfoundComponent } from './auth/notfound/notfound.component';
import { AccountComponent } from './pages/account/account.component';
import { ReportModalComponent } from './pages/report-modal/report-modal.component';
import { ReportDataComponent } from './pages/report-data/report-data.component';
import { UploadReportComponent } from './pages/upload-report/upload-report.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    NotfoundComponent,
    AccountComponent,
    ReportModalComponent,
    ReportDataComponent,
    UploadReportComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AgGridModule.withComponents([])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
