import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-report-data',
  templateUrl: './report-data.component.html',
  styleUrls: ['./report-data.component.css']
})
export class ReportDataComponent implements OnInit {


  public loggedIn: boolean = false;
  public surveys: any;
  public error = null;
  public success = null;
  public token: any;
  public subadmins: any;
  public username = '';
  public profile: any;
  public pic: any | undefined;

  constructor(
    private Jarwis: JarwisService,
    private router: Router,
    private Token: TokenService,
  ) { this.router.routeReuseStrategy.shouldReuseRoute = () => false; }

  openSurvey(name: string) {
    this.router.navigate(['/report', name]);
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  ngOnInit() {

    this.Jarwis.getSurveys().subscribe((response: any) => {
      this.surveys = response['surveys'];
    })

    this.token = localStorage.getItem('token');
    this.subadmins = this.Token.payload(this.token);
    this.username = this.subadmins.data.name;

    let myFormData = new FormData();
    myFormData.append('name', this.username);

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
      console.log(this.pic);
    },
      error => this.handleError(error),
    );

  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
