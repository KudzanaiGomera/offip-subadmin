import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  public loggedIn: boolean = false;
  public error: any;
  public success = null;
  public token: any;
  public subadmins: any;
  public username = '';
  public selectedFile: any = null;
  public profile: any;
  public pic: any | undefined;
  progress = 0;

  public form = {
    newName: '',
    email: '',
    password: '',
  }

  constructor(
    private Jarwis: JarwisService,
    private router: Router,
    private Token: TokenService,
  ) { this.router.routeReuseStrategy.shouldReuseRoute = () => false; }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  onSubmit() {

    this.token = localStorage.getItem('token');
    this.subadmins = this.Token.payload(this.token);
    this.username = this.subadmins.data.name;

    let myFormData = new FormData();
    myFormData.append('newName', this.form.newName);
    myFormData.append('email', this.form.email);
    myFormData.append('password', this.form.password);
    myFormData.append('name', this.username);

    this.Jarwis.subAdminAccount(myFormData).subscribe(
      success => this.handleSuccess(success),
      error => this.handleError(error),
    );
  }

  onUpload() {

    this.token = localStorage.getItem('token');
    this.subadmins = this.Token.payload(this.token);
    this.username = this.subadmins.data.name;

    this.progress = 0;

    if (this.selectedFile === null) {
      this.error = "Please select an image.";
    }
    else {
      let myFormData = new FormData();
      myFormData.append('image', this.selectedFile, this.selectedFile.name);
      myFormData.append('name', this.username);

      this.Jarwis.upload(myFormData).subscribe(
        (event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          }
        },
        error => this.handleError(error)
      );
    }
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.subadmins = this.Token.payload(this.token);
    this.username = this.subadmins.data.name;

    let myFormData = new FormData();
    myFormData.append('name', this.username);

    this.Jarwis.getSubAdminAccount(myFormData).subscribe((response: any) => {
      this.profile = response['admin'];
    });

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
    },
      error => this.handleError(error),
    );
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
