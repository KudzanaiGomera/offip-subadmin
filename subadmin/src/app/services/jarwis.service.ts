import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JarwisService {
  get_all_user() {
    throw new Error('Method not implemented.');
  }

  private baseUrl = 'https://offip.co.za/Backend/api';

  constructor(
    private http: HttpClient,
  ) { }

  loggedIn() {
    return localStorage.getItem('token');
  }

  login(data: any) {
    return this.http.post(`${this.baseUrl}/subadminLogin.php`, data)
  }

  getFooter() {
    return this.http.get(`${this.baseUrl}/getLooter.php`)
  }

  getLogo() {
    return this.http.get(`${this.baseUrl}/getLogo.php`)
  }

  getSurvey() {
    return this.http.get(`${this.baseUrl}/getSurvey.php`);
  }

  getSurveys() {
    return this.http.get(`${this.baseUrl}/getSurveys.php`);
  }

  getSurveyReport(data: any) {
    return this.http.post(`${this.baseUrl}/getSurveyReport.php`, data);
  }

  getSurveyBySurvey(data: any) {
    return this.http.post(`${this.baseUrl}/getSurveyBySurvey.php`, data);
  }

  getAccount(data: any) {
    return this.http.post(`${this.baseUrl}/getAccount.php`, data);
  }

  getSubAdminAccount(data: any) {
    return this.http.post(`${this.baseUrl}/getSubAdminAccount.php`, data);
  }

  subAdminAccount(data: any) {
    return this.http.post(`${this.baseUrl}/subAdminAccount.php`, data);
  }

  upload(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/subAdminUpload.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  getImage(data: any) {
    return this.http.post(`${this.baseUrl}/getSubAdminImage.php`, data)
  }

  uploadReport(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/uploadReport.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  getReports(data: any) {
    return this.http.post(`${this.baseUrl}/getReports.php`, data)
  }

  deleteReport(data: any) {
    return this.http.post(`${this.baseUrl}/deleteReport.php`, data)
  }

}
